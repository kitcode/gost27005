"""exprotect URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from remedy import views as remedy_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', remedy_views.index_view),
    url(r'^step2/$', remedy_views.step2_view),
    url(r'^step3/$', remedy_views.step3_view),
    url(r'^about/$', remedy_views.about_view),
    url(r'^remedy/', include('remedy.urls', namespace='remedy')),
]
