from django.apps import AppConfig


class RemedyConfig(AppConfig):
    name = 'remedy'
    verbose_name = 'Средства защиты'
