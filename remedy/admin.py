from django.contrib import admin
from remedy.models import Remedy,\
    ThreatSource, FirstStep, Step2Item, ProtectClass, ThreatSourceClass, TypeActiveSystem, ExampleVulnerability, \
    Countermeasure


class RemedyAdmin(admin.ModelAdmin):
    list_display = 'certificate_number', 'title', 'certification_organisation', 'declarer',
    list_display_links = 'title',
    search_fields = 'title',
    list_filter = 'active',
    filter_horizontal = 'threat_sources',


admin.site.register(ProtectClass)
admin.site.register(Remedy, RemedyAdmin)
admin.site.register(ThreatSource)
admin.site.register(ThreatSourceClass)
admin.site.register(FirstStep)
admin.site.register(Step2Item)
admin.site.register(TypeActiveSystem)
admin.site.register(ExampleVulnerability)
admin.site.register(Countermeasure)
