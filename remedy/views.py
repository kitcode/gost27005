import random

from django.http import HttpResponse, Http404
from django.http import JsonResponse
from django.shortcuts import render_to_response, redirect, render

from remedy.forms import FirstStepForm
from remedy.models import Remedy, FirstStep, Step2Item, ThreatSourceClass, Countermeasure
import xlrd
import copy


def index_view(request):
    form = FirstStepForm()

    if request.POST:
        form = FirstStepForm(request.POST)

        if form.is_valid():
            first_step = form.save()
            first_step.generate_step()
            first_step.res_number = random.randint(4, 7)
            first_step.save()

            request.session['first_step_id'] = first_step.id
            return redirect('/step2/')

    return render(request, 'index.html', {
        'form': form
    })


def step2_view(request):
    if 'first_step_id' in request.session:
        first_step = FirstStep.objects.get(id=int(request.session['first_step_id']))
        step2_items = Step2Item.objects.filter(first_step=first_step)

        return render(request, 'step2.html', {
            'first_step': first_step,
            'step2_items': step2_items
        })

    raise Http404


def step3_view(request):
    if 'first_step_id' in request.session:
        first_step = FirstStep.objects.get(id=int(request.session['first_step_id']))

        countermeasures = Countermeasure.objects.filter(type_active_system=first_step.type_active_system)

        return render(request, 'step3.html', {
            'first_step': first_step,
            'countermeasures': countermeasures
        })
    raise Http404


def about_view(request):
    return render(request, 'about.html')


def parser(request):

    rb = xlrd.open_workbook('remedy/temp/_reestr_sszi.xlsx')
    sheet = rb.sheet_by_index(0)
    for rownum in range(2, sheet.nrows):
        row = sheet.row_values(rownum)

        print(row)

        Remedy.objects.create(
            certificate_number=row[0],
            title=row[3],
            destination=row[4],
            certification_organisation=row[7],
            declarer=row[8],
            declarer_requisites=row[9]
        )

    return HttpResponse('ok')
