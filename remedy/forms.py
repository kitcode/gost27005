from django import forms
from remedy.models import FirstStep


class FirstStepForm(forms.ModelForm):
    class Meta:
        model = FirstStep
        fields = '__all__'
        widgets = {
            'expert_name': forms.TextInput(attrs={'placeholder': 'Введите ФИО эксперта, привлекаемого для оценки СВР '
                                                                 'угроз ИБ и СТП нарущения ИБ'}),
            'organisation_name': forms.TextInput(attrs={'placeholder': 'Введите название исследуемой организации БС РФ '
                                                                       '/ исследуемого подразлеления организации БС '
                                                                       'РФ'}),
            'system_name': forms.TextInput(attrs={'placeholder': 'Название рассматриваемой системы'}),
            'active_name': forms.TextInput(attrs={'placeholder': 'Название актива'}),
        }

    def clean(self):
        cleaned_data = super(FirstStepForm, self).clean()
        expert_name = cleaned_data.get("expert_name")
        organisation_name = cleaned_data.get("organisation_name")
        system_name = cleaned_data.get("system_name")

        if expert_name is None:
            self.add_error('expert_name', 'Введите ФИО эксперта')
            raise forms.ValidationError('Введите ФИО эксперта')

        if organisation_name is None:
            self.add_error('organisation_name', 'Введите название организации / подразлеления')
            raise forms.ValidationError('Введите название организации / подразлеления')

        if system_name is None:
            self.add_error('budget', 'Введите название рассматриваемой системы')
            raise forms.ValidationError('Введите название рассматриваемой системы')
