import random

from django.db import models


class ProtectClass(models.Model):
    title = models.CharField(verbose_name='Название', max_length=250)

    class Meta:
        verbose_name_plural = 'Классы защищенности'
        verbose_name = 'Класс защищенности'

    def __str__(self):
        return self.title


class ThreatSourceClass(models.Model):
    title = models.CharField(verbose_name='Название', max_length=250)

    class Meta:
        verbose_name_plural = 'Классы источников угроз ИБ'
        verbose_name = 'Класс источников угроз ИБ'

    def __str__(self):
        return self.title


class ThreatSource(models.Model):
    title = models.CharField(verbose_name='Название', max_length=250)
    threat_source_class = models.ForeignKey(ThreatSourceClass, verbose_name='Класс источников угроз ИБ')
    description = models.TextField(verbose_name='Описание')
    protect_class = models.ManyToManyField(ProtectClass, verbose_name='Классы защищённости')

    class Meta:
        verbose_name_plural = 'Источники угрозы ИБ'
        verbose_name = 'Источник угрозы ИБ'

    def __str__(self):
        return self.title


class Remedy(models.Model):
    certificate_number = models.CharField(verbose_name='№ сертификата', max_length=250, null=True, blank=True)
    title = models.CharField(verbose_name='Название', max_length=250)
    destination = models.TextField(verbose_name='Предназначение')
    certification_organisation = models.CharField(verbose_name='Орган по сертификации', max_length=250, null=True,
                                                  blank=True)
    declarer = models.CharField(verbose_name='Заявитель', max_length=250, null=True, blank=True)
    declarer_requisites = models.TextField(verbose_name='Реквизиты заявителя (индекс, адрес, телефон)', null=True,
                                           blank=True)

    active = models.BooleanField(verbose_name='Активный', default=False)
    threat_sources = models.ManyToManyField(ThreatSource, verbose_name='Защищаемые источники угроз', blank=True)

    protect_class = models.ManyToManyField(ProtectClass, verbose_name='Классы защищённости')

    class Meta:
        verbose_name_plural = 'Средства защиты'
        verbose_name = 'Средство защиты'

    def __str__(self):
        return self.title


class TypeActiveSystem(models.Model):
    title = models.CharField(verbose_name='Название', max_length=250)

    class Meta:
        verbose_name_plural = 'Виды активов системы'
        verbose_name = 'Вид актива системы'

    def __str__(self):
        return self.title


class FirstStep(models.Model):
    expert_name = models.CharField(verbose_name='ФИО эксперта', max_length=250)
    organisation_name = models.CharField(verbose_name='Название организации / подразлеления', max_length=250)
    system_name = models.CharField(verbose_name='Название рассматриваемой системы', max_length=250)

    active_name = models.CharField(verbose_name='Название актива', max_length=250)
    type_active_system = models.ForeignKey(TypeActiveSystem, verbose_name='Вид актива системы')

    res_number = models.IntegerField(verbose_name='Количество балов', null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Объкеты Шага 1'
        verbose_name = 'Шаг 1'

    def generate_step(self):
        examples = ExampleVulnerability.objects.filter(type_active_system=self.type_active_system)
        for example in examples:
            Step2Item.objects.create(
                first_step=self,
                example_vulnerability=example
            )
        pass


class ExampleVulnerability(models.Model):
    type_active_system = models.ForeignKey(TypeActiveSystem, verbose_name='Вид актива системы')
    title = models.CharField(verbose_name='Название', max_length=250)
    example_danger = models.CharField(verbose_name='Пример угрозы', max_length=250)

    class Meta:
        verbose_name_plural = 'Примеры уязвимостей'
        verbose_name = 'Пример уязвимости'

    def __str__(self):
        return self.title


class SelectVulnerabilityLevels:
    high = 'high'
    middle = 'middle'
    low = 'low'

    names = {
        'high': 'Высокий',
        'middle': 'Средний',
        'low': 'Низкий',
    }

SELECT_VULNERABILITY_LEVELS_CHOICES = (
    (SelectVulnerabilityLevels.high, SelectVulnerabilityLevels.names[SelectVulnerabilityLevels.high]),
    (SelectVulnerabilityLevels.middle, SelectVulnerabilityLevels.names[SelectVulnerabilityLevels.middle]),
    (SelectVulnerabilityLevels.low, SelectVulnerabilityLevels.names[SelectVulnerabilityLevels.low]),
)


class Step2Item(models.Model):
    first_step = models.ForeignKey(FirstStep)

    example_vulnerability = models.ForeignKey(ExampleVulnerability, verbose_name='Пример уязвимости')
    level_vulnerability = models.CharField(verbose_name='Степень простоты использования уязвимости', max_length=250,
                                           choices=SELECT_VULNERABILITY_LEVELS_CHOICES, null=True, blank=True)
    level_danger = models.CharField(verbose_name='Степень вероятности возникновения угрозы', max_length=250,
                                    choices=SELECT_VULNERABILITY_LEVELS_CHOICES, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Объкеты пунктов Шага 2'
        verbose_name = 'Пункт Шага 1'


class Countermeasure(models.Model):
    type_active_system = models.ManyToManyField(TypeActiveSystem, verbose_name='Вид актива системы')
    title = models.CharField(verbose_name='Мероприятие', max_length=250)
    percent = models.FloatField(verbose_name='Процент', null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.percent:
            self.percent = random.uniform(15, 69)
        super(Countermeasure, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Контрмеры'
        verbose_name = 'Контрмера'
